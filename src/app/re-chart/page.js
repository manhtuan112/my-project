"use client";

import React from 'react';
import { Col, Row } from 'antd';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, LineElement, Title, Tooltip, Legend, PointElement, LineController, BarController } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import CommondTable from '../../components/table';

// Register components
ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    PointElement,
    LineController,
    BarController
);

const SalesChart = () => {
    const data = {
        labels: ['Jan', 'Feb', 'Mar'],
        datasets: [
            {
                label: 'Xe oto bán được',
                data: [2000, 4000, 3000],
                backgroundColor: '#007bff',
                yAxisID: 'y-axis-1',
            },
            {
                label: 'Xe máy bán được',
                data: [7000, 5000, 9000],
                backgroundColor: '#ff5733',
                yAxisID: 'y-axis-1',
            },
            {
                label: '% số xe bán được trong cả năm',
                data: [30, 30, 40],
                type: 'line',
                borderColor: '#28a745',
                backgroundColor: '#28a745',
                yAxisID: 'y-axis-2',
            },
        ],
    };

    const options = {
        scales: {
            'y-axis-1': {
                type: 'linear',
                position: 'left',
                ticks: {
                    beginAtZero: true,
                    max: 10000,
                },
            },
            'y-axis-2': {
                type: 'linear',
                position: 'right',
                ticks: {
                    beginAtZero: true,
                    max: 50,
                    callback: function (value) {
                        return value + '%';
                    },
                },
                grid: {
                    display: false,
                },
            },
        },
        plugins: {
            legend: {
                display: true,
                position: 'bottom', // Move legend to the bottom
            },
            title: {
                display: true,
                text: 'Sales Data',
            },
        },
    };

    return (
        <>
            <Row justify="center">
                <h1>Biểu đồ doanh số bán hàng</h1>
            </Row>
            <Row justify="center">
                <Col xs={24} md={14} xl={12}>
                    <Bar data={data} options={options} width={2} height={1} />
                </Col>
            </Row>
            <Row justify="center">
                <Col xs={24} md={12} xl={10} style={{ marginTop: '20px' }}>
                    <CommondTable />
                </Col>
            </Row>
        </>
    );

};

export default SalesChart;