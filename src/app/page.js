import Link from "next/link";
import styles from "./page.module.css";

export default function Home() {
  return (
    <div>
      <h1 className={styles.title}>Welcome to Next.js!</h1>
      <nav>
        <ul>
          <li>
            <Link href="/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link href="/re-chart">Re Chart</Link>
          </li>
            
        </ul>
      </nav>
    </div>
  );
}
