"use client";

import { Col, Row } from 'antd';
import CommondTable from '../../components/table';
import React from 'react';
import {
  ComposedChart,
  Bar,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';


import { fakeData } from '../../data/fake_data';


// Custom Legend Renderer
// const renderLegend = (props) => {
//   const { payload } = props;
//   return (
//     <ul style={{ listStyleType: 'none', display: 'flex', justifyContent: 'center', padding: 0 }}>
//       {payload.map((entry, index) => (
//         <li key={`item-${index}`} style={{ margin: '10 10px' }}>
//           <span
//             style={{
//               display: 'inline-block',
//               width: entry.type === 'line' ? '20px' : '10px',
//               height: entry.type === 'line' ? '5px' : '10px',
//               backgroundColor: entry.color,
//               marginRight: '5px',
//               borderRadius: entry.type === 'line' ? '0' : '50%',
//             }}
//           />
//           {entry.value}
//         </li>
//       ))}
//     </ul>
//   );
// };

const SalesChart = () => {
  const data = fakeData.map((item) => {
    return {
      name: item.month,
      oto: item.oto,
      may: item.may,
      percent: item.percent,
    }
  })

  return (
    <>
      <Row justify="center">
        <h1>Biểu đồ doanh số bán hàng</h1>
      </Row>
      <Row justify="center">
        <Col xs={24} md={14} xl={12}>
          <ResponsiveContainer width="100%" height={500}>
            <ComposedChart data={data} margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis yAxisId="left" orientation="left" stroke="#8884d8" domain={[10000, 0]} ticks={[0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]}
                interval={0} />
              <YAxis
                yAxisId="right"
                orientation="right"
                stroke="#82ca9d"
                domain={[45, 0]}
                ticks={[0, 5, 10, 15, 20, 25, 30, 35, 40, 45]}
                tickFormatter={(value) => `${value}%`}
                interval={0}
              />
              <Tooltip />
              <Legend verticalAlign="bottom" />
              <Bar yAxisId="left" dataKey="oto" fill="#007bff" name="Xe oto bán được" />
              <Bar yAxisId="left" dataKey="may" fill="#ff5733" name="Xe máy bán được" />

              <Line yAxisId="right" type="linear" dataKey="percent" stroke="#28a745" strokeWidth={3} dot={false} name="% số xe bán được trong cả năm" />
            </ComposedChart>
          </ResponsiveContainer>
        </Col>
      </Row>


      <Row justify="center">
        <Col xs={24} md={12} xl={10} style={{ marginTop: '20px' }}>
          <CommondTable />
        </Col>
      </Row>
    </>
  );
};

export default SalesChart;

// nguyen tac luoi grid: co 4 muc: xs(mobile), sm(tablet), md(medium-monitor), lg(large-monitor 1200px)
// Khi do gia tri se ap dung cho gia tri lon hon hoac bang gia tri do, neu nho hon thi se lay gia tri cua muc nho hon hoac chiem toan bo man hinjh
// Neu khong dinh nghia VD(col-6) thi se ap dung voi moi khung hinh
// VD: col-md-6 col-lg-4 col-sm-3 => tuc la khi man hinh lon hon 1200 se chiem 3/12, man hinh nho chiem 4/12. tablet chiem 6/12 va dien thoai chiem 12/12
