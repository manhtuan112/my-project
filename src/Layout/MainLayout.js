"use client";

import React, { useEffect, useState } from 'react'
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons'
import { AiOutlineDashboard, AiOutlineShoppingCart, AiOutlineUser, AiOutlineBgColors } from 'react-icons/ai'
import { ImBlog } from 'react-icons/im'
import { IoIosNotifications } from 'react-icons/io'
import { Layout, Menu, Button, theme } from 'antd'
const { Header, Sider, Content } = Layout

function MainLayout() {
  const [collapsed, setCollapsed] = useState(false)
  const {
    token: { colorBgContainer }
  } = theme.useToken()

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className='demo-logo-vertical'>
          <h2 className='text-white fs-5 text-center py-3 mb-0'>
            <span className='sm-logo'>TS</span>
            <span className='lg-logo'>T Store</span>
          </h2>
        </div>
        <Menu
          theme='light'
          mode='inline'
          items={[
            {
              key: '',
              icon: <AiOutlineDashboard className='fs-4' />,
              label: 'Dashboard'
            },
            {
              key: 'customers',
              icon: <AiOutlineUser className='fs-4' />,
              label: 'Customers'
            },
          ]}
        />
      </Sider>
      <Layout className='site-layout'>
        <Header
          className='d-flex justify-content-between ps-1 pe-5'
          style={{
            padding: 0,
            background: colorBgContainer
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed)
          })}
          <div className='d-flex gap-4 align-items-center'>
            <div className='position-relative'>
              <IoIosNotifications className='fs-4' />
              <span className='badge bg-warning rounded-circle p-1 position-absolute'>3</span>
            </div>

            <div className='d-flex gap-3 align-items-center dropdown'>
              <div>
                <img
                  width={32}
                  height={32}
                  src='https://firebasestorage.googleapis.com/v0/b/uploadimagetest-d03e3.appspot.com/o/image%2F17566e7e-0e42-43cb-8be5-ab704fa4cf86Nguy%E1%BB%85n%20M%E1%BA%A1nh%20Tu%C3%A2n%20(1)%20(1).JPG?alt=media&token=b3b3dfa4-f1e7-44df-b2d3-22286d402d28'
                  alt=''
                />
              </div>
              <div role='button' id='dropdownMenuLink' data-bs-toggle='dropdown' aria-expanded='false'>
                <h5 className='mb-0'>tuan112</h5>
                <p className='mb-0'>manhtuan1122001@gmail.com</p>
              </div>
            </div>
          </div>
        </Header>
      </Layout>
    </Layout>
  )
}

export default MainLayout
