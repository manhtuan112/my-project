import React from 'react'
import { Table } from "antd";
import { fakeData } from '../data/fake_data';

function CommondTable() {
  const dataSource = [
    {
      key: '1',
      name: 'Xe oto bán được',
    },
    {
      key: '2',
      name: 'Xe máy bán được',
    },
    {
      key: '3',
      name: '% số xe bán được trong cả năm',

    }
  ];

  fakeData.forEach((item, index) => {
    dataSource[0][item.month] = item.oto;
    dataSource[1][item.month] = item.may;
    dataSource[2][item.month] = `${item.percent}%`;
  });

  const columns = [
    {
      title: '',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Jan',
      dataIndex: 'Jan',
      key: 'Jan',
    },
    {
      title: 'Feb',
      dataIndex: 'Feb',
      key: 'Feb',
    },
    {
      title: 'Mar',
      dataIndex: 'Mar',
      key: 'Mar',
    },
  ];


  return (
    <Table dataSource={dataSource} columns={columns} pagination={false} />
  )
}

export default CommondTable